class InvalidArgument(Exception):
    "Function called with invalid arguments"
    pass

def max_dist(a = None):
    if type(a) != list or len(a) < 2:
        raise InvalidArgument
    if (type(a[0]) != int) or (type(a[1]) != int):
        raise TypeError
    result = abs(a[0] - a[1])
    for i in range(1,len(a)-1):
        if (type(a[i+1]) != int):
            raise TypeError
        r = abs(a[i] - a[i+1])
        if r > result:
            result = r
    return result
