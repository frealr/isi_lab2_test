from calendar import isleap

class InvalidArgument(Exception):
    "Function called with invalid arguments"
    pass


def day_of_year(*args):
    if (len(args) != 3):
        raise InvalidArgument

    if (type(args[0]) != int) or (type(args[1]) != int) or (type(args[2]) != int):
        raise InvalidArgument
    valid_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if (args[2] < 0) or (args[1] not in range(1,13)) or (args[0] not in range(1,valid_days[args[1]-1]+1)):
        if not (isleap(args[2]) and (args[0] == 29)):
            raise InvalidArgument
    result = 0
    for i in range(1,args[1]):
        result += valid_days[i-1]
        if (i == 2) and (isleap(args[2])):
            result += 1
    result += args[0]
    return result
