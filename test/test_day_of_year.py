from day_of_year import day_of_year, InvalidArgument
import pytest

def test_A():
    with pytest.raises(InvalidArgument):
        assert day_of_year()

def test_B():
    with pytest.raises(InvalidArgument):
        assert day_of_year("1",11,2022)

def test_C():
    with pytest.raises(InvalidArgument):
        assert day_of_year(32,4,2021)

def test_D():
    assert 339 == day_of_year(4, 12, 2016)


def test_E():
    assert 60 == day_of_year(29, 2, 2008)



