from max_dist import max_dist, InvalidArgument
import pytest

def test_A():
    with pytest.raises(InvalidArgument):
        assert max_dist()

def test_B():
    with pytest.raises(TypeError):
        assert max_dist([1,"a"])

def test_B1():
    with pytest.raises(TypeError):
        assert max_dist([1, 1, "a"])


def test_C():
    assert max_dist([1,2,8,7]) == 6
