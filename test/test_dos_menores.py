from dos_menores import dos_menores
import pytest

@pytest.mark.parametrize(["argumento","salida_esperada"], [("hola",None),([],None),(None,None),
                                                           ([1,2],(1,2)),([1],1),
                                                           ([2,1],(1,2)),
                                                           ([1,1],(1,1)), ([1,1,1],(1,1)),
                                                           ([1,2,3,4],(1,2)), ([2,3,1],(1,2)),
                                                           ([1,2,3],(1,2)), ([1,3,2],(1,2)),
                                                           ])
def test_dos_menores(argumento, salida_esperada):
    assert dos_menores(argumento) == salida_esperada